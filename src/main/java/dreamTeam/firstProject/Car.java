package dreamTeam.firstProject;

/**
 * Created by Ксюшка on 02.10.2016.
 */
public interface Car {

    /**
     * opens car
     * @param key key to open car with
     * @return is car opened
     */
    boolean open(String key);

    /**
     * closes car
     * @return is car closed
     */
    boolean close(String key);

    /**
     * fill car with oil
     * @param oilType type of oil
     * @param quantity quantity of oil in litres
     * @return was filled with this type of oil and this quantity
     */
    boolean fill(String oilType, int quantity);

    /**
     * moves car
     * @param speed with which speed to move
     * @return is car moves
     */
    boolean move(int speed);
}
