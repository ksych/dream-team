package dreamTeam.firstProject;

/**
 * Created by Ксюшка on 02.10.2016.
 */
public interface Printer {

    /**
     * prints message
     * @param message message to print
     * @return was message printed
     */
    boolean print(String message);

    /**
     * adds paper to print
     * @param paperType type of paper
     * @param quantity quantity of paper sheet
     * @return was paper added
     */
    boolean addPaper(String paperType, int quantity);

    /**
     * adds int to printer
     * @param color color of inc
     * @param quantity quantity of inc
     * @return was inc added
     */
    boolean addInk(String color, int quantity);
}
