package dreamTeam.realisations;
import dreamTeam.firstProject.Car;

public class BMW implements Car
{

    /**
     *
     */
    boolean isOpen = false;

    /**
     *
     */
    String secretTokenOfKey;

    /**
     * Speed of car
     */
    int speed;

    /**
     * Level of oil
     */
    int levelOfOil;


    private static final int MAX_LEVEL_OIL = 100;

    /**
     *
     */
    public BMW(String secretTokenOfKey)
    {
        this.secretTokenOfKey = secretTokenOfKey;
        this.speed = 0;
        this.levelOfOil = 0;
    }

    /**
     *
     * @return
     */
    public int getLevelOfOil() {
        return levelOfOil;
    }

    /**
     *
     * @param levelOfOil
     */
    public void setLevelOfOil(int levelOfOil) {
        this.levelOfOil = levelOfOil;
    }

    /**
     * Get current speed
     * @return
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * Set speed for car
     * @param speed
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * Function for check moving of car
     * @param speed
     * @return
     */
    public boolean move(int speed)
    {
        this.speed  = speed;
        return speed < 0 ? false : true;
    }

    /**
     * Function to check type of oil for BMW
     * @param oilType
     * @return
     */
    protected boolean goodOil(String oilType)
    {
        switch (oilType) {
            case "95": return true;
            case "94": return true;
            case "92": return false;
            default: return false;
        }
    }

    @Override
    public boolean open(String secretTokenOfKey) {
        if (!this.isOpen && this.secretTokenOfKey.equals(secretTokenOfKey)) {
            this.isOpen = true;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean close(String secretTokenOfKey)
    {
        if (this.isOpen && this.secretTokenOfKey.equals(secretTokenOfKey)) {
            this.isOpen = false;
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param oilType
     * @param quantity
     * @return
     */
    public boolean fill(String oilType, int quantity)
    {
        if (this.goodOil(oilType) &&
            this.levelOfOil < this.MAX_LEVEL_OIL &&
                quantity > 0
        ) {
            this.levelOfOil += quantity;
            return true;
        } else {
            return false;
        }
    }
}