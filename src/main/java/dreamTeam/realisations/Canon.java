package dreamTeam.realisations;
import dreamTeam.firstProject.Printer;

import java.util.ArrayList;


public class Canon implements Printer
{

    /**
     *
     */
    ArrayList<String> arrayList;

    /**
     *
     */
    int countOfPaper;

    /**
     *
     */
    int levelOfRed;

    /**
     *
     */
    int levelOfGreen;

    /**
     *
     */
    int levelOfBlue;

    /**
     *
     */
    private static final int MAX_COUNT_OF_PAPER = 1000;

    /**
     *
     */
    private static final int MAX_LEVEL_RED = 100;

    /**
     *
     */
    private static final int MAX_LEVEL_GREEN = 100;

    /**
     *
     */
    private static final int MAX_LEVEL_BLUE = 100;


    /**
     *
     * @param typeOfPaper
     * @return
     */
    public boolean addTypeOfPaper(String typeOfPaper)
    {
        if (this.arrayList != null) {
            if (this.arrayList.indexOf(typeOfPaper) == -1) {
                this.arrayList.add(typeOfPaper);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     *
     * @param typeOfPaper
     * @return
     */
    private boolean checkTypeOfPaper(String typeOfPaper)
    {
        if (this.arrayList != null) {
            if (this.arrayList.indexOf(typeOfPaper) == -1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     *
     */
    public Canon()
    {
        this.arrayList = new ArrayList<>();
        this.addTypeOfPaper("A4");
        this.addTypeOfPaper("A5");

        this.levelOfRed = 100;
        this.levelOfGreen = 100;
        this.levelOfBlue = 100;
        this.countOfPaper = 0;
    }

    /**
     * prints message
     * @param message message to print
     * @return was message printed
     */
    public boolean print(String message)
    {
        if (this.countOfPaper > 0 && this.levelOfRed > 0
            && this.levelOfGreen > 0 && this.levelOfBlue >0)
        {
            this.countOfPaper--;
            return true;
        } else {
            return false;
        }
    }

    /**
     * adds paper to print
     * @param paperType type of paper
     * @param quantity quantity of paper sheet
     * @return was paper added
     */
    public boolean addPaper(String paperType, int quantity)
    {
        if (this.checkTypeOfPaper(paperType)) {
            if (quantity <= 0) {
                return false;
            }
            this.countOfPaper += quantity;
            if (this.countOfPaper > this.MAX_COUNT_OF_PAPER) {
                this.countOfPaper = this.MAX_COUNT_OF_PAPER;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * adds int to printer
     * @param color color of inc
     * @param quantity quantity of inc
     * @return was inc added
     */
    public boolean addInk(String color, int quantity)
    {
        if (quantity <= 0) {
            return false;
        }

        switch (color) {
            case "red" : {
                this.levelOfRed += quantity;
                if (this.levelOfRed > this.MAX_LEVEL_RED) {
                    this.levelOfRed = this.MAX_LEVEL_RED;
                }
                break;
            }

            case "green": {
                this.levelOfGreen += quantity;
                if (this.levelOfGreen > this.MAX_LEVEL_GREEN) {
                    this.levelOfGreen = this.MAX_LEVEL_GREEN;
                }
                break;
            }

            case "blue": {
                this.levelOfBlue += quantity;
                if (this.levelOfBlue > this.MAX_LEVEL_BLUE) {
                    this.levelOfBlue = this.MAX_LEVEL_BLUE;
                }
                break;
            }

            default: {
                return false;
            }
        }
        return true;
    }
}