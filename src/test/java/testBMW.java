import dreamTeam.realisations.BMW;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.*;

public class testBMW {
    BMW bmw;

    @Before
    public void createCar(){
        bmw = new BMW("openTheDoor");
    }

    @Test
    public void testMove1() throws Exception{
        assertTrue("true", bmw.move(4));
    }

    @Test
    public void testMove2() throws Exception{
        assertTrue("false", bmw.move(0));
    }

    @Test
    public void testMove3() throws Exception{
        assertFalse("negative meaning", bmw.move(-4));
    }

    @Test
    public void testOpen1() throws Exception{
        assertFalse("Block Door", bmw.open("NotFunded"));
    }

    @Test
    public void testOpen2() throws Exception{
        assertTrue("Open Door", bmw.open("openTheDoor"));
    }

    @Test
    public void testClose1() throws Exception{
        bmw.open("openTheDoor");
        assertTrue("Close Door", bmw.close("openTheDoor"));
    }

    @Test
    public void testClose2() throws Exception{
        bmw.open("openTheDoor");
        assertFalse("Not Close Door", bmw.close("NorFunded"));
    }

    @Test
    public void testFill1() throws Exception{
        assertTrue("Good fill", bmw.fill("95", 4));
    }

    @Test
    public void testFill2() throws Exception{
        assertFalse("Wrong number of gasoline", bmw.fill("95", 0));
    }
    @Test
    public void testFill3() throws Exception{
        assertFalse("Wrong number of gasoline", bmw.fill("95", -4));
    }
    @Test
    public void testFill4() throws Exception{
        assertFalse("Not the gasoline", bmw.fill("98", 4));
    }
}
