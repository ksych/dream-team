import dreamTeam.realisations.Canon;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.*;

public class testCanon {
    Canon canon;

    @Before
    public void createPrinter(){
        canon = new Canon();
        //canon.countOfPaper = 100;
    }

    @Test
    public void testAddTypeOfPaper1(){
        assertTrue("True type", canon.addTypeOfPaper("A1"));
    }

    @Test
    public void testAddTypeOfPaper2(){
        assertTrue("True type", canon.addTypeOfPaper("A12"));
    }

    @Test
    public void testAddTypeOfPaper3(){
        assertFalse("Repetition type", canon.addTypeOfPaper("A5"));
    }

    @Test
    public void testPrint1(){
        canon.addPaper("A8", 10);
        assertTrue("Message printed", canon.print("Hello World!!"));
    }

    @Test
    public void testPrint2(){
        canon.addPaper("A8", 10);
        assertTrue("Null message printed", canon.print(""));
    }

    @Test
    public void testAddPaper1(){
        assertTrue("Good add paper", canon.addPaper("A8", 10));
    }
    @Test
    public void testAddPaper2(){
        assertFalse("Not inserted paper", canon.addPaper("A8", 0));
    }

    @Test
    public void testAddInk1(){

        assertTrue("Add red collor", canon.addInk("red", 100));
    }

    @Test
    public void testAddInk2(){
        assertTrue("Add green collor", canon.addInk("green", 100));

    }
    @Test
    public void testAddInk3(){
        assertTrue("Add blue collor", canon.addInk("blue", 100));
    }

    @Test
    public void testAddInk4(){
        assertFalse("Adding another color", canon.addInk("yellow", 100));
    }

    @Test
    public void testAddInk5(){
        assertFalse("We do not add color", canon.addInk("red", 0));
    }
}
